const { Routes } = require("discord-api-types/v9");
const { Client, Intents } = require("discord.js");
const { REST } = require("@discordjs/rest");
const { SlashCommandBuilder } = require('@discordjs/builders');
const Voice = require('@discordjs/voice');
const fs = require('fs');
require('dotenv').config()

let soundlist;
const client = new Client({ intents: [Intents.FLAGS.GUILDS, Intents.FLAGS.GUILD_MESSAGES, Intents.FLAGS.GUILD_VOICE_STATES] });
const connections = {};
const commands = [
	new SlashCommandBuilder().setName('memes').setDescription('Join current voice channel'),
	new SlashCommandBuilder().setName('nomemes').setDescription('Leave voice channel'),
	new SlashCommandBuilder().setName('random').setDescription('Play random meme right now!'),
].map(command => command.toJSON());
const player = Voice.createAudioPlayer({
    behaviors: {
        noSubscriber: Voice.NoSubscriberBehavior.Play,
    },
})
const rest = new REST({ version: "9" }).setToken(process.env.DISCORD_TOKEN);

(async () => {
  try {
    console.log("Started refreshing application [/] commands.");

    await rest.put(
      Routes.applicationCommands(process.env.CLIENT_ID),
      { body: commands },
    );

    console.log("Successfully reloaded application [/] commands.");
  } catch (error) {
    console.error(error);
  }
})();

fs.readdir('./sounds', (err, files) => {
    soundlist = files;
    loadMemesLoop(player);
});

client.once("ready", () => {
    console.log("I'm ready !");
    client.user.setPresence({ activities: [{ name: 'memes', type: "STREAMING", url: "https://www.twitch.tv/jackfilov" }], status: 'dnd' });
});

client.on("interactionCreate", async (interaction) => {
    if (!interaction.isCommand()) return;
    if (interaction.commandName === "memes") {
        if (!interaction.member.voice.channelId) return await interaction.reply({ content: "You are not in a voice channel, dummy!", ephemeral: true });
        
        connections[interaction.guildId] = Voice.joinVoiceChannel({
            channelId: interaction.member.voice.channelId,
            guildId: interaction.guildId,
            adapterCreator: interaction.guild.voiceAdapterCreator,
        });
        
        connections[interaction.guildId].subscribe(player);

        return await interaction.reply({ content: "Enjoy! :)", ephemeral: true });
    
    } else if (interaction.commandName === "nomemes"){
        if (!interaction.guild.me.voice.channelId) return await interaction.reply({ content: "I'm not in a voice channel bruh", ephemeral: true });
    
        if (connections[interaction.guildId]) connections[interaction.guildId].destroy();
    
        return await interaction.reply({ content: "ok done", ephemeral: true });
    } else if (interaction.commandName === "random"){
        if (!interaction.guild.me.voice.channelId) return await interaction.reply({ content: "I'm not in a voice channel bruh", ephemeral: true });
        console.log(interaction.member.displayName, "has used /random in", interaction.guild.name);
        playRandomMeme(player);
        return await interaction.reply({ content: "I did the funny", ephemeral: true });
    } else {
        return await interaction.reply({ content: "I don't know that command", ephemeral: true });
    }
});


function playRandomMeme(player){
    const randID = Math.floor(Math.random() * soundlist.length);
    const resource = Voice.createAudioResource(`./sounds/${soundlist[randID]}`);
    console.log("Played", soundlist[randID]);
    player.play(resource);
}

function loadMemesLoop(player){
    playRandomMeme(player);
    setTimeout(loadMemesLoop, Math.random() * 5 * 60 * 1000 + 60000, player);
}

client.login(process.env.DISCORD_TOKEN);