# MemeSoundsDiscord
Have you ever been so lonely and desperate for this one annoying friend who just blasts Soundpad sounds from time to time?

Well, it's your lucky day since now there's a bot for that!

Simply add it [here](https://discord.com/oauth2/authorize?client_id=952509511334260776&permissions=0&scope=bot%20applications.commands) and *enjoy* memes every 1-6 minutes.

## How to use

* /memes - to invite the bot to your voice channel
* /nomemes - politely ask the bot to leave

## How to host

1. Run `npm install`.
2. Put your `DISCORD_TOKEN` and `CLIENT_ID` inside `.env` file. (rename example file `.env.example`) 
3. Put your funny meme sounds inside `sounds` folder. (yes, you have to create one)
4. Run the bot with `node .` or `npm start`.
